use serde_json::{Result, Value};

pub fn parse_json() -> Result<()> {

    let data = r#"
    {
        "Stock": "X-Trade Brokers",
        "Symbol": "CMR",
        "Owners": [
            "Janusz Filipiak",
            "Unknow"
        ]
    }"#;

    let v: Value = serde_json::from_str(data)?;

    println!("On the {} exchange, you can find {} owned by {} and {}", v["Stock"], v["Symbol"], v["Owners"][0], v["Owners"][1]);

    Ok(())
}