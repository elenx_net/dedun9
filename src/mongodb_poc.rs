use mongodb::options::ClientOptions;
use mongodb::Client;
use mongodb::{bson::doc, bson::Document, options::FindOptions};
use futures::TryStreamExt;
type Error = Box<dyn std::error::Error>;

pub async fn mongo_poc() -> Result<(),  Error> {
    let mut client_options = ClientOptions::parse("mongodb://localhost:27017").await?;
    let client = Client::with_options(client_options)?;
    let db = client.database("mydb");

    let collection = db.collection::<Document>("books");

    let docs = vec![
        doc! { "title": "1984", "author": "George Orwell" },
        doc! { "title": "Animal Farm", "author": "George Orwell" },
        doc! { "title": "The Great Gatsby", "author": "F. Scott Fitzgerald" },
    ];
    collection.insert_many(docs, None).await?;

    let filter = doc! { "author": "George Orwell" };
    let find_options = FindOptions::builder().sort(doc! { "title": 1 }).build();
    let mut cursor = collection.find(filter, find_options).await?;

    while let Some(book) = cursor.try_next().await? {
        println!("title: {}", book.get("title").unwrap());
    }

    Ok(())
}
