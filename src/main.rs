mod crawler;

type Error = Box<dyn std::error::Error>;
use futures::stream::StreamExt;
use futures::pin_mut;
use crate::crawler::Crawler;

use async_stream::stream;

use futures_core::stream::Stream;
use reqwest::Response;

#[tokio::main]
async fn main() -> Result<(),  Error> {
    let client = reqwest::Client::new();
    let crawler = Crawler::new(client);
    let etf_stream = crawler.download_etf().await;
    pin_mut!(etf_stream);

    let mut counter = 1;
    while let Some(value) = etf_stream.next().await {
        println!("{} {}", counter, match value {
            Err(e) => "ERROR".to_string(),
            Ok(ok) => ok.status().to_string()
        });
        counter+=1;
    }
    Ok(())
}