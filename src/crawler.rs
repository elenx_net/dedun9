use async_stream::try_stream;
use async_stream::stream;
use futures::{pin_mut, FutureExt};
use futures_core::stream::Stream;
use reqwest::{Response, Client, RequestBuilder, Request};
use select::document::Document;
use select::predicate::{Class, Attr};
use futures::{TryStreamExt};
use std::collections::HashMap;
use serde_json::{json, Value, Error};
use std::{iter, error};
use futures::stream::{self, StreamExt};
use std::ops::Range;
use std::iter::Map;
use tokio;
use std::fmt;
use tokio::task::{JoinHandle, JoinError};

const CONCURRENT_REQUESTS: usize = 50;

#[derive(Debug)]
pub enum CrawlingError {
    HttpError(reqwest::Error),
    Custom(String),
}

type CrawlingResult<T> = std::result::Result<T, CrawlingError>;

impl std::error::Error for CrawlingError {}

impl fmt::Display for CrawlingError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            CrawlingError::HttpError(err) => err.fmt(f),
            CrawlingError::Custom(message) => write!(f, "{}", message),
        }
    }
}

impl From<reqwest::Error> for CrawlingError {
    fn from(reqwest_error: reqwest::Error) -> Self {
        CrawlingError::HttpError(reqwest_error)
    }
}

pub struct Crawler {
    http_client: Client,
}

impl Crawler {
    pub fn new(client: Client) -> Self {
        Crawler {
            http_client: client,
        }
    }

    pub async fn download_etf(&self) -> impl Stream<Item=CrawlingResult<Response>> + '_ {
        let overview_page = self.http_client.get("https://etfdb.com/screener/#tab=overview")
            .send().await.expect("Cannot fetch tabs")
            .text().await.expect("Cannot parse response to text");

        let metadata = self.http_client.post("https://etfdb.com/api/screener/")
            .json(&json!({"only": ["meta"]}))
            .send().await.expect("Cannot fetch total_pages")
            .json::<HashMap<String, Value>>().await.expect("Cannot parse total_pages to Map<String, Value>");

        let total_pages = metadata
            .get("meta").expect("Missing 'meta' in json total_pages response")
            .as_object().expect("Cannot parse 'meta' to Value object")
            .get("total_pages").expect("Missing 'total_pages' in Value object")
            .as_u64().expect("Cannot parse 'total_pages' to integer value")
            as u16;

        let requests = Document::from(&overview_page[..])
            .find(Attr("data-screener-navigation", ""))
            .next().expect("Missing 'data-screener-navigation' attribute, cannot fetch tab names")
            .children()
            .filter_map(|li| li.attr("data-screener-tab-target"))
            .flat_map(|tab_name| self.to_request(tab_name, total_pages))
            .collect();

        self.send_async(requests)
    }

    fn to_request(&self, tab: &str, pages: u16) -> Vec<Request> {
        (1..(pages + 1)).map(|page|
            self.http_client.post("https://etfdb.com/api/screener/")
                .json(&Crawler::create_json(tab, page))
                .build().expect("Cannot build Request"))
            .collect()
    }

    fn create_json(tab: &str, page: u16) -> Value {
        json!({
            "tab": tab.to_string(),
            "page": page,
            "only": ["data"]
        })
    }

    fn send_async(&self, requests: Vec<Request>) -> impl Stream<Item=CrawlingResult<Response>> + '_ {
        futures::stream::iter(requests.into_iter()
            .map(move |request| {
                let client = self.http_client.clone();
                tokio::spawn(async move {
                    client.execute(request).await
                })
            }))
            .buffer_unordered(CONCURRENT_REQUESTS)
            .map(|future_response| match future_response {
                Ok(Ok(res)) => Ok(res),
                Ok(Err(e)) => Err(CrawlingError::HttpError(e)),
                Err(_e) => Err(CrawlingError::Custom("Cannot join handle".to_string())),
            })
    }
}