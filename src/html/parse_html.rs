use select::document::Document;
use select::predicate::{Attr, Name};

pub fn parse_by_row() {
    let document = Document::from(include_str!("etfdb.html"));

    for node in document.find(Name("tr")).take(10)  {
        println!("Data by row: {}", node.text());
    };
}

pub fn parse_by_column() {
    let document = Document::from(include_str!("etfdb.html"));
    
    for node in document.find(Attr("data-th", "Price")).take(10)  {
        println!("Price: {}", node.text());
    };

    for node in document.find(Attr("data-th", "Assets"))  {
        println!("Assets: {}", node.text());
    };
}